-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           5.7.24 - MySQL Community Server (GPL)
-- SE du serveur:                Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour fake_news_2
CREATE DATABASE IF NOT EXISTS `fake_news_2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fake_news_2`;

-- Listage de la structure de la table fake_news_2. posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `content` mediumtext,
  `category` varchar(50) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `picture` varchar(50) DEFAULT '',
  `alt_picture` varchar(50) DEFAULT '',
  `chapo` varchar(250) DEFAULT '',
  `subtitle` varchar(250) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table fake_news_2.posts : ~4 rows (environ)
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` (`id`, `title`, `content`, `category`, `date`, `picture`, `alt_picture`, `chapo`, `subtitle`) VALUES
	(8, 'CERISIER ALIEN', '<p><span class="bold-b">Fake News</span> - Bonjour Bob, que ressentez-vous maintenant que tout le monde connaît votre existence? <br></p>\r\n<p><span class="bold-b">Bob</span> - Et bien, à vrai dire nous sommes plutôt soulagés. Certains d\'entre nous ont failli devenir fous à force de se faire butiner par des insectes en tout genre.<br></p>\r\n<p><span class="bold-b">Fake News</span> - Comment et quand êtes-vous arrivés sur Terre ?<br></p>\r\n<p><span class="bold-b"Bob></span> -Il y a bien longtemps, dans une galaxie ! Mine, très lointaine, nous avons commencé notre voyage d\'exploration Intel-dimensionnel à la recherche d\'autres formes de vie. En arrivant près de votre Jupiter, les forces gravitaionnelles ont déréglé le convecteur temporel et notre vitesse est passé en dessous des 88 MO_ .Nous avons été obligés d\'allumer les propulseurs hyperdnve pour ne pas nous faire aspirer par la planète géante. Malheureusement, suite à une surchauffe, nous avons terminé notre course sur votre Terre, en plot milieu d\'un Champs de censiers en fleurs Quelques autotochtones, attirés parle bruit arnvèrentarmésde fourches et de torches. Effrayés par la menace nous avons improvisé un plan de camouflage_ En effet, nos respirateurs ressemblent étrangement aux fleurs de vos censiers, nous avons donc grimpé aux arbres pour nous cacher permis les fleurs Cela Nt maintenant 800 ans que nous sommes ici. Voyant que la menace état partie, nous avons décidé de réveler notre existence<br></p>\r\n<p><span class="bold-b">Fake News</span> - Fascinant ! Merci pour votre témoignage Bob, et bienvenu par parmi nous.<br></p>', 'fake-news', '2020-07-14 12:18:00', 'images/pic03.jpg', 'Fake news fraîche', '<span class="bold-a">EXCLUSIF !</span> les aliens sont parmi nous ! Ils se cachent dans les cerisiers déguisés en fleurs. L\'<a href="/" class="fnews">interview exclusive de Bob l\'extraterrestre.</a>', ''),
	(9, 'Huile de palmipède', '<p><span class="bold-b">Fake News</span> - Bonjour Bob, que ressentez-vous maintenant que tout le monde connaît votre existence? <br></p>\r\n<p><span class="bold-b">Bob</span> - Et bien, à vrai dire nous sommes plutôt soulagés. Certains d\'entre nous ont failli devenir fous à force de se faire butiner par des insectes en tout genre.<br></p>\r\n<p><span class="bold-b">Fake News</span> - Comment et quand êtes-vous arrivés sur Terre ?<br></p>\r\n<p><span class="bold-b"Bob></span> -Il y a bien longtemps, dans une galaxie ! Mine, très lointaine, nous avons commencé notre voyage d\'exploration Intel-dimensionnel à la recherche d\'autres formes de vie. En arrivant près de votre Jupiter, les forces gravitaionnelles ont déréglé le convecteur temporel et notre vitesse est passé en dessous des 88 MO_ .Nous avons été obligés d\'allumer les propulseurs hyperdnve pour ne pas nous faire aspirer par la planète géante. Malheureusement, suite à une surchauffe, nous avons terminé notre course sur votre Terre, en plot milieu d\'un Champs de censiers en fleurs Quelques autotochtones, attirés parle bruit arnvèrentarmésde fourches et de torches. Effrayés par la menace nous avons improvisé un plan de camouflage_ En effet, nos respirateurs ressemblent étrangement aux fleurs de vos censiers, nous avons donc grimpé aux arbres pour nous cacher permis les fleurs Cela Nt maintenant 800 ans que nous sommes ici. Voyant que la menace état partie, nous avons décidé de réveler notre existence<br></p>\r\n<p><span class="bold-b">Fake News</span> - Fascinant ! Merci pour votre témoignage Bob, et bienvenu par parmi nous.<br></p>', 'fake-news', '2020-07-26 09:14:00', 'images/pic02.jpg', 'Fake news fraîche', 'Des chercheurs ont découvert qu\'à cause de l\'huile de palme quelle contient, une consommation excessive de pâte à tartine provoquerait une mutation du pied en pâte de canard.<a href="/" class="fnews"> Les photos exclusives ici !.</a>', ''),
	(10, 'COMMENT RANGER UN LIVRE ?', '<p><span class="bold-b">Fake News</span> - Bonjour Bob, que ressentez-vous maintenant que tout le monde connaît votre existence? <br></p>\r\n<p><span class="bold-b">Bob</span> - Et bien, à vrai dire nous sommes plutôt soulagés. Certains d\'entre nous ont failli devenir fous à force de se faire butiner par des insectes en tout genre.<br></p>\r\n<p><span class="bold-b">Fake News</span> - Comment et quand êtes-vous arrivés sur Terre ?<br></p>\r\n<p><span class="bold-b"Bob></span> -Il y a bien longtemps, dans une galaxie ! Mine, très lointaine, nous avons commencé notre voyage d\'exploration Intel-dimensionnel à la recherche d\'autres formes de vie. En arrivant près de votre Jupiter, les forces gravitaionnelles ont déréglé le convecteur temporel et notre vitesse est passé en dessous des 88 MO_ .Nous avons été obligés d\'allumer les propulseurs hyperdnve pour ne pas nous faire aspirer par la planète géante. Malheureusement, suite à une surchauffe, nous avons terminé notre course sur votre Terre, en plot milieu d\'un Champs de censiers en fleurs Quelques autotochtones, attirés parle bruit arnvèrentarmésde fourches et de torches. Effrayés par la menace nous avons improvisé un plan de camouflage_ En effet, nos respirateurs ressemblent étrangement aux fleurs de vos censiers, nous avons donc grimpé aux arbres pour nous cacher permis les fleurs Cela Nt maintenant 800 ans que nous sommes ici. Voyant que la menace état partie, nous avons décidé de réveler notre existence<br></p>\r\n<p><span class="bold-b">Fake News</span> - Fascinant ! Merci pour votre témoignage Bob, et bienvenu par parmi nous.<br></p>', 'fake-news', '2020-07-28 08:57:00', 'images/pic01.jpg', 'Une fake news de dingue', 'On vous ment <span class="bold-a">depuis le début</span>, il faut ranger les livres sur la tranche, c\'est meilleur pour leur santé mentale. ', ''),
	(11, 'DES FAUX PASSEPORTS POUR LES ALIENS', '<p><span class="bold-b">Fake News</span> - Bonjour Bob, que ressentez-vous maintenant que tout le monde connaît votre existence? <br></p>\r\n<p><span class="bold-b">Bob</span> - Et bien, à vrai dire nous sommes plutôt soulagés. Certains d\'entre nous ont failli devenir fous à force de se faire butiner par des insectes en tout genre.<br></p>\r\n<p><span class="bold-b">Fake News</span> - Comment et quand êtes-vous arrivés sur Terre ?<br></p>\r\n<p><span class="bold-b"Bob></span> -Il y a bien longtemps, dans une galaxie ! Mine, très lointaine, nous avons commencé notre voyage d\'exploration Intel-dimensionnel à la recherche d\'autres formes de vie. En arrivant près de votre Jupiter, les forces gravitaionnelles ont déréglé le convecteur temporel et notre vitesse est passé en dessous des 88 MO_ .Nous avons été obligés d\'allumer les propulseurs hyperdnve pour ne pas nous faire aspirer par la planète géante. Malheureusement, suite à une surchauffe, nous avons terminé notre course sur votre Terre, en plot milieu d\'un Champs de censiers en fleurs Quelques autotochtones, attirés parle bruit arnvèrentarmésde fourches et de torches. Effrayés par la menace nous avons improvisé un plan de camouflage_ En effet, nos respirateurs ressemblent étrangement aux fleurs de vos censiers, nous avons donc grimpé aux arbres pour nous cacher permis les fleurs Cela Nt maintenant 800 ans que nous sommes ici. Voyant que la menace état partie, nous avons décidé de réveler notre existence<br></p>\r\n<p><span class="bold-b">Fake News</span> - Fascinant ! Merci pour votre témoignage Bob, et bienvenu par parmi nous.<br></p>', 'fake-news', '2020-05-14 01:34:00', 'images\\pic03.jpg', 'des faux passeports pour les Aliens', 'Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet mollis tortor congue. Sec quis mauris sit amet magna accumsan tristique.', '');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;

-- Listage de la structure de la table fake_news_2. settings
CREATE TABLE IF NOT EXISTS `settings` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `ss_titre_trucs` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `ss_titre_accueil` mediumtext CHARACTER SET utf8mb4 NOT NULL,
  `value` varchar(250) CHARACTER SET utf8mb4 NOT NULL DEFAULT '0',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Listage des données de la table fake_news_2.settings : ~1 rows (environ)
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` (`Id`, `ss_titre_trucs`, `ss_titre_accueil`, `value`) VALUES
	(1, 'voiture', 'velo', '0');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;

-- Listage de la structure de la table fake_news_2. users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `FK_users_role` (`role`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Listage des données de la table fake_news_2.users : ~9 rows (environ)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `login`, `password`, `role`) VALUES
	(2, 'admin', 'admin', 'administrateur'),
	(5, 'Mr chat', 'sourie', 'administrateur'),
	(8, 'uyuy', ' ', 'administrateur'),
	(12, 'yy', 'yy', 'administrateur'),
	(17, 'uu', 'uu', 'administrateur'),
	(21, 'Voiture', ' ', 'redacteur'),
	(22, 'steph', 'steph', ''),
	(25, 'Mme sourie', 'chaton', 'redacteur');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
