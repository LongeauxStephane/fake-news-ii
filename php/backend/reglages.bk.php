<?php 
    include 'navigation.bk.php';

    require_once '../includes/functions.php';
    require_once '../includes/database.php';

    if (!isset($_POST['ss-title']) ){
        $_POST['ss-title']= null;        
    } 
?>

<body class="backend">
    
<section class="pg-reglages">
    <h1>Réglages</h1>
    <h2>Sous-titres</h2>

    <h3>Page</h3>
    <div class="row">

        <div class="col-lg-6 col-md-6 col-6">
            <form action="" method="POST">
                <select name="ss-title">
                    <option value="">Page</option>
                    <option value="ss-title-accueil" name="ss-title--accueil">Accueil</option>
                    <option value="ss-title-trucs" name="ss-title--trucs">Trucs en Toc</option>
                    
                </select>
                <button type="submit">Valider</button>
            </form>
        </div>      
        
        <div class="col-lg-6 col-md-6 col-6">   

            <!-- Sous-titre Accueil -->
            <?php if( $_POST['ss-title'] == 'ss-title-accueil'):   
                echo '                        
                    <span>Sous-titre actuel: </span>
                    <br/> ' .
                    $ss_title_accueil . '
                    <br/>
                    <form action="" method="POST" class="form-ss-titres">
                        <input type="text" name="set-ss-title-accueil">
                        <button type="submit">Valider</button>
                    </form>
                ';
            ; endif ?>        

            <!-- Sous-titre Trucs en Toc -->
            <?php if( $_POST['ss-title'] == 'ss-title-trucs'):   
                echo '                        
                    <span>Sous-titre actuel: </span>
                    <br/> ' .
                    $ss_title_trucs . '
                    <br/>
                    <form action="" method="POST" class="form-ss-titres">
                        <input type="text" name="set-ss-title-trucs">
                        <button type="submit">Valider</button>
                    </form>
                ';
            ; endif ?>                                  
                                
        </div>
                          
     

    </div>

  


  


</section>

</body>