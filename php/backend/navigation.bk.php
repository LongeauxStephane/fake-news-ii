<?php
    session_start();
    require_once '../includes/database.php'; 

    require_once '../includes/constants.php';
    include 'header.bk.php';    
?>


<nav class="bk-nav">
    <div class="bk-title">
        <a href="homepage.bk.php">
            <?php echo TITLE ?>
        </a>           
    </div>    
    <ul class="bk-menu">    
        <li class="bk-link">
            <a href="reglages.bk.php">Réglages</a>                
        </li>
        <?php if ($_SESSION['role'] == 'administrateur'): echo '
        <li class="bk-link ss-link">
            <a href="#" class="collapsed">Utilisateurs</a>
                <ul class="ss-menu bk-ss-menu">
                    <li>
                        <a href="users.php">Liste des utilisateurs</a>
                    </li>
                    <li>
                        <a href="users_add.php">Ajouter</a>
                    </li>                      
                </ul>
        </li>'
        ; endif ?>
        <li class="bk-link ss-link">
            <a href="#"  class="collapsed">Articles</a>
            <ul class="ss-menu bk-ss-menu">
                <li>
                    <a href="articles.php">Liste des articles</a>
                </li>
                <li>
                    <a href="articles_add.php">Ajouter</a>
                </li>                 
            </ul>
        </li>
        <li class="bk-link">
            <?php echo $msg_deconnexion;
            ?>            
        </li>
    </ul>
</nav>



<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script type="application/javascript" src="../../js/script.js"></script>