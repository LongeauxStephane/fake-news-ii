<?php

require_once '../includes/functions.php';
?>


<body class="backend">

<div class="main-panel">

    <div class="row">
    <?php if ($_SESSION['role'] == 'administrateur'): echo '
        <div class="col-lg-6 col-md-6 col-6">
            <div class="statistiques">
                <div class="users">
                    <span class="onglet-title">
                        Nombres d\'utilisateurs
                    </span>
                    <div class="user-count"> ' .
                        $users_count['total_users'] . '
                    </div>
                </div>
            </div>
        </div>'
        ; endif ?>

        <div class="col-lg-6 col-md-6 col-6">
            <div class="statistiques">
                <div class="users">
                    <span class="onglet-title">
                        Nombres d'articles
                    </span>
                    <div class="user-count">
                        <?php echo $articles_count['total_articles']; ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
   
  


</div>

</body>
