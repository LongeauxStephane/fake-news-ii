<?php 
    include 'navigation.bk.php';

    require_once '../includes/functions.php';
?>

<body class="backend">


<section class="pg-articles">
    <h1>Liste des articles</h1>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="head-section">
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-1">
                        <span></span>
                    </div>
                    <div class="col-lg-1 col-md-1 col-1">
                        <span>Id</span>
                    </div>
                    <div class="col-lg-7 col-md-7 col-7">
                        <span>Titre</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Date</span>
                    </div>                    
                </div>
            </div>
        </div>        
    </div>
    <div class="tableau">
        <form action="" method="GET">
            <div class="row">            
                <div class="col-lg-12 col-md-12 col-12">
                     <?php articles_list('bk_articles'); ?>
                </div>           
            </div>
            <div class="modifier-row">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-2">

                            <?php echo '                            
                                <button type="submit" name="btn-modifier-art" id="btn-modifier-art">
                                    Modifier                                    
                                </button>                               
                            ' ;                                
                            ?>
                                  
                       
                    </div>
                    <div class="col-lg-2 col-md-2 col-2">
                        <button type="" name="btn-ajouter-art">Ajouter</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-2">
                        <button type="submit" name="btn-supprimer-art">Supprimer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>

    
</body>



