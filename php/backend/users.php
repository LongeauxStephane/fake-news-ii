<?php 
    include 'navigation.bk.php';

    require_once '../includes/functions.php';
?>


<body class="backend">

<section class="pg-users">
    <h1>Utilisateurs</h1>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="head-section">
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-1">
                        <span>Selection</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Id</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Nom utilisateur</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Status</span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="tableau">
        <form action="" method="GET" class="users-list">
            <div class="row">            
                <div class="col-lg-12 col-md-12 col-12">
                    <?php users_list(); ?>
                </div>           
            </div>
            <div class="modifier-row">
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-2">

                            <?php echo '                            
                                <button type="submit" name="btn-modifier" id="btn-modifier">
                                    Modifier                                    
                                </button>                               
                            ' ;                            
                            ?>
                                  
                       
                    </div>
                    <div class="col-lg-2 col-md-2 col-2">
                        <button type="" name="btn-ajouter">Ajouter</button>
                    </div>
                    <div class="col-lg-2 col-md-2 col-2">
                        <button type="submit" name="btn-supprimer">Supprimer</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>




</body>