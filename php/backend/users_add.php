<?php 
    include 'navigation.bk.php';

    require_once '../includes/functions.php';
?>

<section class="pg-users-added">
    <h1>Utilisateurs ajouté</h1>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
            <div class="head-section">
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-1">
                        <span>Liste des utilisateurs</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Id</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Nom utilisateur</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Status</span>
                    </div>
                </div>
            </div>
        </div>        
    </div>
    <div class="tableau">   
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <?php users_list(); ?>
            </div> 
        </div> 
        <div>
            <?php users_add (); ?>
        </div>
    </div>
</section>