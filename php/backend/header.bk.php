<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/dist/style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fda18f4986.js" crossorigin="anonymous"></script>
    <title>Fake News II, Reloaded</title>
</head>

    <nav class="nav-info-bar">
        <div class="info-bar">
            <?php echo '<p class="salutation">Bonjour, '  . $_SESSION['user'] . ' | ' . $_SESSION['role'] .'</p>'; ?>
        </div>
    </nav>