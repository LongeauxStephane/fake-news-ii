<?php 
    include 'navigation.bk.php';

    require_once '../includes/functions.php';
?>



<section class="pg-users-modifier">
<h1>Utilisateurs à modifier</h1>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-12">
        <div class="head-section">
                <div class="row">                                     
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Nom utilisateur</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span>Status</span>
                    </div>
                </div>
            </div>
        </div>  
        </div>        
    </div>
    <div class="tableau">
        
            <div class="row">            
                <div class="col-lg-12 col-md-12 col-12">
                    <?php users_list_modifier (); ?>
                </div>           
            </div>
            <div class="modifier-row">
                <?php users_form_modifier(); ?>               
            </div>
        
    </div>
</section>
          

