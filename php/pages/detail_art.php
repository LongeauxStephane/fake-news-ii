<?php 
    include '../includes/header.php';
    
?>


<main class="pg-detail"> 

    <div class="pg-head">
        <?= TITLE ?>
        <h1><?= $posts_title ?></h1>
    </div>

    <!--Separation-->
    <div class="separateur sep-hd-main"></div>
   


    <?php 
    function display_article() {    

        if ( isset($_GET['id'])  ){ 
                   
            $id = $_GET['id'];            

            $servername = "localhost";
            $username = "root";
            $password = "";

            $conn = new mysqli($servername, $username, $password, 'fake_news_2');    
            $query_posts = "SELECT * FROM posts WHERE id= '$id'";
            $rslt_posts = mysqli_query($conn, $query_posts);

            while ( $rows = mysqli_fetch_assoc($rslt_posts)) {
               
                $posts_id = $rows['id'];
                $posts_title = utf8_encode($rows["title"]);
                $posts_content = utf8_encode($rows["content"]);
                $posts_category = $rows["category"];
                $post_date =  $rows['date'];
                $posts_date = new DateTime("$post_date"); 
                $date_format =  date_format($posts_date, 'd/m/Y H:i');
                $posts_picture = $rows["picture"];            
                $posts_alt_picture = $rows["alt_picture"]; 
                $url_picture = 'http://localhost:8000/' . $posts_picture;   
                $posts_chapo = utf8_encode($rows["chapo"]);  

                $_SESSION['page-title'] = $posts_title;
                
                echo <<< POSTS_LIST
                
                <div class="col-lg-12 col-md-12 col-12">
               
                        <article class="card">                    
                        <div class="date">
                            <span name="art_date"> Publié le $date_format</span>
                        </div>
                            <div class="cont-img">
                                <img src="$url_picture" name="art_img">
                            </div>      
                            <h3 class="titre" name="art_title"></h3>
                            <div class="cont-art-desc">
                                <span class="art-desc">
                                    $posts_content
                                </span>
                            </div>
                        </article>
                          
                   
                </div>  
              
            POSTS_LIST;
            }
        }
        

   
        
    } 
?>

<?php display_article(); ?>

</main>






<?php 
    include '../includes/footer.php';
?>

