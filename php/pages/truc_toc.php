<?php 
    include '../includes/header.php';
    require_once '../backend/utils/fonctionnalites.bk.php'; 
    
?>

    <!--Titre-->
    <div class="cont-title">
        <?php echo TITLE ?>
        <h1>Truc en toc</h1>
            <span class="ss-titre titre">
                <?= $ss_title_trucs ?>
            </span>
    </div>

    <main class="pg-truc pg-articles"> 

        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <?php articles_list (); ?>
            </div>
        </div>

    </main>
    
<?php 
    include '../includes/footer.php';
?>

