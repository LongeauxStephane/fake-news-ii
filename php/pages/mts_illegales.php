<?php 
    include '../includes/header.php';    
?>

<h2>
    Données personnelles :
</h2>

<p>
    Les informations que la médiathèque de Meurthe-et-Moselle est amenée à recueillir proviennent de leur communication volontaire par les personnes physiques ou morales par saisie sur les formulaires disponibles en ligne. Le caractère facultatif ou obligatoire des données est indiqué sur chaque formulaire.
    
    La médiathèque Meurthe-et-Moselle s'engage à ce que la collecte et le traitement de données à caractère personnel soient conformes à la loi n°78-17 du 6 janvier 1978 modifiée relative à l'informatique, aux fichiers et aux libertés. Les différentes données collectées ne font l'objet d'aucune cession à des tiers de la part de la médiathèque. Les informations liées au compte usager sont conservées conformément aux règles prescrites par la loi de 1978 et pendant une durée justifiée par la finalité de leur traitement, sont réservées à l'usage de la Médiathèque et ne peuvent être communiquées qu'aux personnels et destinataires habilités.
    
     Vos droits Informatique et Libertés et la façon de les exercer auprès de la CNIL.
</p>

<h2>
    Exercer vos droits :
</h2>

<p>
    Pour toute information ou exercice de vos droits Informatique et Libertés sur les traitements de données personnelles gérés par la Médiathèque de Meurthe-et-Moselle, vous pouvez contacter son délégué à la protection des données (DPO) :
    
        soit par écrit  : le demandeur adresse un courrier signé accompagné de la copie d’un titre d’identité à l'adresse suivante :
    
    Monsieur le délégué à la protection des données personnelles (DPD/DPO) :
    
    Conseil départemental de Meurthe-et-Moselle
    
    Monsieur Le Délégué à La Protection des Données
    
    48 Esplanade Jacques-Baudot
    
    CO 90019
    
    54035 NANCY CEDEX
    
        soit par courriel : dpo@departement54.fr
</p>

 <h2>
     
    Cookies
 </h2>

<p>
    « Ce site utilise Google Analytics, un service d'analyse de site internet fourni par Google Inc. (« Google »). Google Analytics utilise des cookies, qui sont des fichiers texte placés sur votre ordinateur, pour aider le site internet à analyser l'utilisation du site par ses utilisateurs. Les données générées par les cookies concernant votre utilisation du site (y compris votre adresse IP) seront transmises et stockées par Google sur des serveurs situés aux Etats-Unis. Google utilisera cette information dans le but d'évaluer votre utilisation du site, de compiler des rapports sur l'activité du site à destination de son éditeur et de fournir d'autres services relatifs à l'activité du site et à l'utilisation d'Internet. Google est susceptible de communiquer ces données à des tiers en cas d'obligation légale ou lorsque ces tiers traitent ces données pour le compte de Google, y compris notamment l'éditeur de ce site. Google ne recoupera pas votre adresse IP avec toute autre donnée détenue par Google. Vous pouvez désactiver l'utilisation de cookies en sélectionnant les paramètres appropriés de votre navigateur. Cependant, une telle désactivation pourrait empêcher l'utilisation de certaines fonctionnalités de ce site. En utilisant ce site internet, vous consentez expressément au traitement de vos données nominatives par Google dans les conditions et pour les finalités décrites ci-dessus. »
</p>
 



<?php 
    include '../includes/footer.php';
?>