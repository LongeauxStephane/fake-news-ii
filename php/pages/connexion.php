<?php 
    include '../includes/header.php';       
?>

<main class="pg-connexion">
    <form action="" method="POST">
            <!--L'utilisateur doit voir qu'il est connecté a la place du formulaire/ -> If connected change page-->
            <div class="form-cont">
                <label for="login">Utilisateur:</label>
                <input type="text" name="login" placeholder="Nom">
                <label for="name">Mot de passe:</label>
                <input type="password" name="password" placeholder="Mot de passe">
            </div>
            <button type="submit" name="btn-submit" class="btn-submit">Connexion</button>
        
    </form>
</main>

<?php 
    include '../includes/footer.php';
?>