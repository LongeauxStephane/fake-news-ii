<?php
    session_start();

    require_once 'constants.php';
    include 'database.php';   
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../../css/dist/style.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/fda18f4986.js" crossorigin="anonymous"></script>
    <title><?php echo  $title?></title>   
</head>
<body>

    <div class="modal-success">
        <p>On vous avoit prévenu qu'il allait faire noir...<br/>
        Votre message en à profité pour s'enfuir jusqu'à chez nous !</p>
        <button class="btn-modal">J'ai peur du noir :'(</button>
    </div>

<div class="container">

    <header>
        <!--Menu principal -->
        <nav id="side-bar" class="nav-main-menu">  
            <!-- Btn Burger menu -->
            <div class="toggle-btn">
                <span class="icon-bar"></span>
               <span class="icon-bar"></span>   
               <span class="icon-bar"></span>   
             </div>       
            <ul class="menu main-menu">
                <li class="link-menu link-rembobiner">
                    <a href="/" class="mn-menu-link">Rambobiner</a>                                          
                </li>
                <li class="link-menu link-truc">
                    <a href="../../php/pages/truc_toc.php" class="mn-menu-link">Trucs en toc</a>                                        
                </li>
                <li class="link-menu link-rouages">
                    <a href="../../php/pages/connexion.php" class="mn-menu-link">Rouages</a>                    
                </li>
            </ul>
        </nav>

    </header>         
 