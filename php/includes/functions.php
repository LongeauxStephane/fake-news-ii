<?php  

    // $dir = dirname(__FILE__);
    // echo $dir; 
    include 'database.php'; 

/*=================================================================================================================*/
/*============================================= GENERIQUES ======================================================*/
/*===============================================================================================================*/
function cnx_db(string $tb_name)
{
    $servername = "localhost";
    $username = "root";
    $password = "";
    $conn = new mysqli($servername, $username, $password, 'fake_news_2');

    $query_db = "SELECT * FROM `$tb_name`";
    $rslt_query = mysqli_query($conn, $query_db);

}

/*=================================================================================================================*/
/*============================================= PANEL STATS =======================================================*/
/*===============================================================================================================*/

/* Users Count*/
$query_total_users = 'SELECT count(*) AS total_users FROM users';
$rslt_total_users = mysqli_query($conn, $query_total_users);
$users_count = mysqli_fetch_assoc($rslt_total_users);

/* Articles Count*/
$query_total_articles = 'SELECT count(*) AS total_articles FROM posts';
$rslt_total_articles = mysqli_query($conn, $query_total_articles);
$articles_count = mysqli_fetch_assoc($rslt_total_articles);


/*=================================================================================================================*/
/*============================================= SOUS-TITRES ======================================================*/
/*===============================================================================================================*/

    $default_ss_title = 'IL REVIENT ET IL EST PAS CONTENT<br/>
    MYTHONÈ EN PHP ET MYSQL.';

    /* Set Sous-titre Accueil*/
    if ( isset($_POST['set-ss-title-accueil'] ) ){        
        $input_ss_title_ac = $_POST['set-ss-title-accueil'];       
        $add_ss_titre = "UPDATE settings SET ss_titre_accueil = '$input_ss_title_ac' WHERE id=1";
        $add_rslt = mysqli_query($conn, $add_ss_titre);   
    } else {       
        $_POST['set-ss-title-accueil'] = $default_ss_title;                                                               
    }

    /* Set Sous-titre Truc*/
    if ( isset($_POST['set-ss-title-trucs'] ) ){        
        $input_ss_title_tc = $_POST['set-ss-title-trucs'];       
        $add_ss_titre = "UPDATE settings SET ss_titre_trucs = '$input_ss_title_tc' WHERE id=1";
        $add_rslt = mysqli_query($conn, $add_ss_titre);   
    } else {       
        $_POST['set-ss-title-accueil'] = $default_ss_title;                                                               
    }
    

/*=================================================================================================================*/
/*============================================= UTILISATEURS =====================================================*/
/*===============================================================================================================*/

/*========== Liste d'utilisateurs =========*/

    /*Utilisateurs selection*/    
    if ( isset($_GET['btn-modifier']) ) {       
        if ( !empty($_GET['user_pick']) ) {
            foreach($_GET['user_pick'] as $picked){    
                $_SESSION['user_picked'] = $picked;                
            }
        }
    }    

    /*Liste utilisateurs*/
    function users_list () { 
        cnx_db('users');
        global $conn; 
        $query_users = "SELECT * FROM users";
        $rslt_users = mysqli_query($conn, $query_users);
        
            while ( $rows = mysqli_fetch_assoc($rslt_users) ) {
                $users_id = $rows['id'];
                $users_login = $rows["login"];
                $users_role = $rows["role"];            
        
                echo <<< USERS_LIST
                    
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-1">
                            <input type="checkbox" id="$users_id" name="user_pick[]" value="$users_login">
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$users_id</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$users_login</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$users_role</span>
                        </div>
                    </div>      
                    
                                
                USERS_LIST;  
    
            }    
    }
    
/*========== Ajout d'utilisateurs =========*/

    /* Ajout d'utilisateurs*/
    if ( isset($_POST['btn_users_add']) ) {      
        $user_login_added = $_POST['user_login_add'];
        $user_status_added = $_POST['user_status_add'];
        $user_password_added = $_POST['user_password_add'];
        $add_user_login = "INSERT INTO users(login,password,role) VALUES('$user_login_added', '$user_password_added', '$user_status_added')";
        $add_user_rslt = mysqli_query($conn, $add_user_login);              
    }   

    function users_add ()
    {
        if ( !isset($_GET["btn-ajouter"]) ) {            
            echo <<< USERS_ADD
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout">
                        <div class="col-lg-10 col-md-10 col-10">
                            <label for="user_login_add">Nom utilisateur</label>
                                <input type="text" name="user_login_add">        
                            <label for="user_password_add">Mot de passer</label>
                                <input type="text" name="user_password_add">        
                            <label for="user_status_add">Status</label>
                            <select name="user_status_add" id="">
                                <option value="administrateur" name="administrateur">Administrateur</option>
                                <option value="redacteur" name="redacteur">Redacteur</option>              
                            </select>  
                            <button type="submit" name="btn_users_add" >Valider</button>      
                        </div>            
                    </form>
                </div>
            USERS_ADD;      
        }

    }

    /* Redirection btn: ajouter utilisateur*/
    if ( isset($_GET['btn-ajouter']) ) {
        header('Location: users_add.php'); 
    }

    /*Liste utilisateurs*/
    function users_list_ajoute ()
    { 
        cnx_db('users');        

        /* Ajout d'utilisateurs*/
        $user_login_added = null;
        $user_status_added = null;
        $user_password_added = null;

        if ( isset($_POST['btn_users_add']) ) {      
            $user_login_added = $_POST['user_login_add'];
            $user_status_added = $_POST['user_status_add'];
            $user_password_added = $_POST['user_password_add'];

            $add_user_login = "INSERT INTO users(login,password,role) VALUES('$user_login_added', '$user_password_added', '$user_status_added')";
            $add_user_rslt = mysqli_query($conn, $add_user_login);              
        }
    
        while ( $rows = mysqli_fetch_assoc($rslt_users) ) {
            $users_id = $rows['id'];
            $users_login = $rows["login"];
            $users_role = $rows["role"];            
    
            echo <<< USERS_LIST_AJOUTER
                
                <div class="row">
                    <div class="col-lg-1 col-md-1 col-1">
                        <input type="checkbox" id="$users_id" name="user_pick[]" value="$users_login">
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span class="cell">$users_id</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span class="cell">$user_login_added</span>
                    </div>
                    <div class="col-lg-3 col-md-3 col-3">
                        <span class="cell">$user_status_added</span>
                    </div>
                </div>   

            USERS_LIST_AJOUTER;  

        }

    }



/*========== Modification d'utilisateurs =========*/

    /* Modification d'utilisateurs*/
    if ( isset($_POST['btn_users_modified']) ) {   
        $user_picked = $_SESSION['user_picked'];
        $user_login_modified = $_POST['user_login_modified'];
        $user_status_modified = $_POST['user_status_modified'];
        $user_password_modified = $_POST['user_password_modified'];         
        $modify_user = "UPDATE users SET `login` = '$user_login_modified', `role` = '$user_status_modified', `password` = '$user_password_modified ' 
        WHERE `login` = '$user_picked'";        
        $add_user_rslt = mysqli_query($conn, $modify_user);                  
    }

    function users_form_modifier ()
    {
        if ( !isset($_GET["btn-ajouter"]) ) {            
            echo <<< USERS_FORM_MODIFIER
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout">
                        <div class="col-lg-10 col-md-10 col-10">
                            <label for="user_login_modified">Nom utilisateur</label>
                                <input type="text" name="user_login_modified">        
                            <label for="user_password_modified">Mot de passer</label>
                                <input type="text" name="user_password_modified">        
                            <label for="user_status_modified">Status</label>
                            <select name="user_status_modified" id="">
                                <option value="administrateur" name="administrateur">Administrateur</option>
                                <option value="redacteur" name="redacteur">Redacteur</option>              
                            </select>  
                            <button type="submit" name="btn_users_modified" >Valider</button>      
                        </div>            
                    </form>
                </div>
            USERS_FORM_MODIFIER;    
        }
        
    }/*Fin fonction*/

    /*Liste utilisateurs a modifier */
    function users_list_modifier () { 
        cnx_db('users');
        global $conn;
        $query_users = "SELECT * FROM users";
        $rslt_users = mysqli_query($conn, $query_users);
    
        while ( $rows = mysqli_fetch_assoc($rslt_users) ) {
            $users_id = $rows['id'];
            $users_login = $rows["login"];
            $users_role = $rows["role"]; 
            
            if ( isset ($_SESSION['user_picked']) ){
                if($_SESSION['user_picked'] ==  $users_login ) {                 
                    echo <<< USERS_LIST_MODIFIER
                        
                        <div class="row">        
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$users_login</span>
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$users_role</span>
                            </div>
                        </div>                           
                                    
                    USERS_LIST_MODIFIER;  


                }
            }   

        }

    }


    /* Redirection btn: modifier -> function dans database */    
    if ( isset($_GET['btn-modifier']) ) {
        header('Location: users_modifier.php');
    }

/*========== Supression d'utilisateurs =========*/ 

    /*Supression d'utilisateurs*/
    if ( isset( $_GET['btn-supprimer']) ) {
        if ( !empty($_GET['user_pick']) ) {
            foreach($_GET['user_pick'] as $picked) { 
                $_SESSION['user_picked'] = $picked;
                $erase_user_query = "DELETE FROM users WHERE `login`= '$picked'";
                $erase_user_rslt = mysqli_query($conn, $erase_user_query);
            }
        }
    }


/*=================================================================================================================*/
/*=============================================== ARTICLES =======================================================*/
/*===============================================================================================================*/


/*========== Liste d'articles =========*/

    /*Article selection*/    
    if ( isset($_GET['btn-modifier-art']) ) {          
        if ( !empty($_GET['post_pick']) ) {
            foreach($_GET['post_pick'] as $picked){    
                $_SESSION['post_picked'] = $picked;                
            }
        }
    }   

/*========== Ajout d'articles =========*/

    function art_form_Add ()
    {
        if ( !isset($_GET["btn-ajouter-art"]) ) {
            
            echo <<< ART_FORM_ADD
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout form-art-mod" id="form-art-mod" enctype="multipart/form-data">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_title_added">Titre</label>
                                        <input type="text" name="post_title_added">    
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_chapo_added">Chapo</label>
                                        <textarea type="textarea" name="post_chapo_added" form="form-art-mod" class="txt-chapo">  </textarea>
                                </div>
                            </div>      
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_category_added">Category</label>
                                    <textarea type="textarea" name="post_category_added" form="form-art-mod" class="txt-content">  </textarea>  
                                </div>
                            </div>              
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_content_added">Content</label>
                                    <textarea type="textarea" name="post_content_added" form="form-art-mod" class="txt-content">  </textarea>  
                                </div>
                            </div>              
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="upload_img">Image</label>
                                    <input type="file" id="upload_img" name="file" accept="image/png, image/jpeg"> 
                                    <button type="submit" name="btn_upload" >Enregistrer</button>                
                                </div>
                            </div>              
                            <button type="submit" name="save_art" >Valider</button>      
                        </div>            
                    </form>
                </div>
            ART_FORM_ADD; 
        }         

    }

    function add_art()
    {
        if ( isset($_POST['save_art']) ) {  
            cnx_db('posts');
            global $conn;

            $post_title_added = $_POST['post_title_added'];
            $post_chapo_added = $_POST['post_chapo_added'];
            $post_category_added = $_POST['post_category_added'];    
            $post_content_added = $_POST['post_content_added'];    

            $ajout_post = "INSERT INTO posts(`title`,`chapo`,`content`, `category`, `date`) VALUES('$post_title_added', '$post_chapo_added', '$post_content_added', '$post_category_added', CURRENT_TIMESTAMP)";        
            $add_art_query = mysqli_query($conn, $ajout_post);                 
            
            mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); 
            var_dump($post_title_added);                 
            var_dump($post_chapo_added);                 
            var_dump($post_content_added);                 
        }

    }

    /* Redirection btn: ajouter articles*/
    if ( isset($_GET['btn-ajouter-art']) ) {
        header('Location: articles_add.php'); 
    }


/*========== Modification d'articles =========*/

    /* Modification d'articles*/
    if ( isset($_POST['btn_art_modified']) ) {   
        $post_picked = $_SESSION['post_picked'];
        $post_title_modified = $_POST['post_title_modified'];
        $post_chapo_modified = $_POST['post_chapo_modified'];
        $post_content_modified = $_POST['post_content_modified'];         
        $modify_post = "UPDATE posts SET `title` = '$post_title_modified', `chapo` = '$post_chapo_modified', `content` = '$post_content_modified' 
        WHERE `id` = '$post_picked'";        
        $mod_post_rslt = mysqli_query($conn, $modify_post);                  
    }  

    /*Liste articles*/
    function articles_list (string $pg_type = '' ) { 
        cnx_db('posts');
        global $conn;
        $query_posts = "SELECT * FROM posts ORDER BY `date` DESC";
        $rslt_posts = mysqli_query($conn, $query_posts);

        if ( $pg_type == 'articles') {
            $query_posts = "SELECT * FROM posts ORDER BY `date` DESC LIMIT 3";
            $rslt_posts = mysqli_query($conn, $query_posts);        
        }

        while ( $rows = mysqli_fetch_assoc($rslt_posts)) {
            $posts_id = $rows['id'];
            $posts_title = utf8_encode($rows["title"]);
            $posts_content = utf8_encode($rows["content"]);                
            $posts_category = $rows["category"];
            $post_date =  $rows['date'];
            $posts_date = new DateTime("$post_date"); 
            $date_format =  date_format($posts_date, 'd/m/Y H:i');
            $posts_picture = $rows["picture"];            
            $posts_alt_picture = $rows["alt_picture"]; 
            $url_picture = 'http://localhost:8000/' . $posts_picture;   
            $posts_chapo = utf8_encode($rows["chapo"]);            

            $article_link = "/php/pages/detail_art.php?id=$posts_id";

            if ( $pg_type == 'articles') {          
                
                echo <<< POSTS_LIST
                    <div class="col-lg-4 col-md-6 col-12">
                    <a href="$article_link">
                            <article class="card">                    
                                <div class="cont-img">
                                    <img src="$url_picture" name="art_img">
                                </div>                          
                                <div class="date">
                                    <span name="art_date">$date_format</span>
                                </div>
                                <h3 class="titre" name="art_title">$posts_title</h3>
                                <div class="cont-art-desc">
                                    <span class="art-desc">
                                        $posts_chapo
                                    </span>
                                </div>
                            </article>
                        </a>
                    </div>  
                
                POSTS_LIST;

            } elseif ( $pg_type == 'bk_articles' ) {
                echo <<< ARTS_LIST
                    <div class="row">
                        <div class="col-lg-1 col-md-1 col-1">
                            <input type="checkbox" id="$posts_id" name="post_pick[]" value="$posts_id">
                        </div>
                        <div class="col-lg-1 col-md-1 col-1">
                            <span class="cell">$posts_id</span>
                        </div>
                        <div class="col-lg-7 col-md-7 col-7">
                            <span class="cell">$posts_title</span>
                        </div>
                        <div class="col-lg-3 col-md-3 col-3">
                            <span class="cell">$date_format</span>
                        </div>
                    </div>                                        
                ARTS_LIST;  

            } else {                   
                echo <<< POSTS_LIST
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12">
                            <a href="#">
                                <article class="card">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="date">
                                                <span>$date_format</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-12">
                                            <div class="">
                                                <h3>$posts_title</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3 col-md-3 col-3">
                                            <div class="cont-img">
                                                <img src="$url_picture">
                                            </div>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-8">
                                            <div class="cont-art-desc">
                                                <span>$posts_content</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="btn-suite">
                                            <a href="$article_link" class="btn-art">Je veux la suite</a>
                                        </div>
                                    </div>
                                </article>
                            </a>
                        </div>
                    </div>
                
                POSTS_LIST;  
            }/*Fin else*/

        }/*Fin while*/

    }/*Fin function users_list*/


    /*Modifier -> function dans database*/    
    if ( isset($_GET['btn-modifier-art']) ) {
        header('Location: articles_modifier.php');
    }

    function art_form_modifier () {
        if ( !isset($_GET["btn-modifier-art"]) ) {            
            echo <<< ART_FORM_MODIFIER
                <div class="row">
                    <form action="" method="POST" class="formulaire-ajout form-art-mod" id="form-art-mod" enctype="multipart/form-data">
                        <div class="col-lg-12 col-md-12 col-12">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_title_modified">Titre</label>
                                        <input type="text" name="post_title_modified">    
                                </div>
                            </div>    
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_chapo_modified">Chapo</label>
                                        <textarea type="textarea" name="post_chapo_modified" form="form-art-mod" class="txt-chapo">  </textarea>
                                </div>
                            </div>      
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="post_content_modified">Content</label>
                                    <textarea type="textarea" name="post_content_modified" form="form-art-mod" class="txt-content">  </textarea>  
                                </div>
                            </div>              
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-12">
                                    <label for="upload_img">Image</label>
                                    <input type="file" id="upload_img" name="file" accept="image/png, image/jpeg"> 
                                    <button type="submit" name="btn_upload" >Enregistrer</button>                
                                </div>
                            </div>              
                            <button type="submit" name="btn_art_modified" >Valider</button>      
                        </div>            
                    </form>
                </div>
            ART_FORM_MODIFIER;      
        }

        if ( isset($_GET["btn_upload"]) ) {
            $file = $_FILES['file'];            

            $file_name = $_FILES['file']['name'];
            $file_TmpName = $_FILES['file']['tmp_name'];
            $file_size = $_FILES['file']['size'];
            $file_error = $_FILES['file']['error'];
            $file_type = $_FILES['file']['type'];

            if ( $file_error === 0 ) {
                $file_destination = '../../uploads/' . $file_name;
                

                move_uploaded_file($file_TmpName, $file_destination);

                $post_id = $_SESSION['post_picked'];  

                $servername = "localhost";
                $username = "root";
                $password = "";
                $conn = new mysqli($servername, $username, $password, 'fake_news_2');  
                $query_upload = "UPDATE posts SET picture = '$file_destination' WHERE id = '$post_id'";
                $upload_rslt = mysqli_query($conn,  $query_upload);                   
            } else {
                echo 'Erreur à l\'envoie';
            }
        }

    }

    /*Liste articles a modifier */
    function article_modifier () { 
        cnx_db('posts');
        global $conn; 
        $query_posts = "SELECT * FROM posts";
        $rslt_posts = mysqli_query($conn, $query_posts);
    
        while ( $rows = mysqli_fetch_assoc($rslt_posts) ) {
            $post_id = $rows['id'];
            $post_title = utf8_encode($rows["title"]);
            $post_date = $rows["date"]; 
            
            if ( isset ($_SESSION['post_picked']) ){
                if($_SESSION['post_picked'] ==  $post_id ) {                 
                    echo <<< USERS_LIST_MODIFIER
                        
                        <div class="row">        
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$post_title</span>
                            </div>
                            <div class="col-lg-3 col-md-3 col-3">
                                <span class="cell">$post_date</span>
                            </div>
                        </div>                       
                                    
                    USERS_LIST_MODIFIER;  
                }
            }
    

        }/*Fin while*/   
             
    }/*Fin function users_list*/




/*========== Suppression d'articles =========*/

    /*Supression d'articles*/
    if ( isset( $_GET['btn-supprimer-art']) ) {
        if ( !empty($_GET['post_pick']) ) {
            foreach($_GET['post_pick'] as $picked) { 
                $_SESSION['post_picked'] = $picked;
                $erase_post_query = "DELETE FROM posts WHERE `id`= '$picked'";
                $erase_post_rslt = mysqli_query($conn, $erase_post_query);
            }
        }
    }


