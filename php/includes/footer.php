
</div><!-- Fin container -->

<footer>

    <div class="container-fluid">
        <div class="container">
            <div class="cont-footer">
                <h2>Des questions ? <strong>Contactez-nous:</strong></h2>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-12"><!-- Liens pratiques -->
                        <div class="row">
                            <div class="cont-pratique">
                                <div class="col-lg-12">
                                    <span class="ss-titre">Nous vous répondons exclusivement par pigeaon voyageur.</span>
                                </div>                            
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <ul class="links-pratiques">
                                                    <li class="link-adresse">
                                                        <a href="#"><!--adresse-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fas fa-home"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                1234 Ch. de la perdition<br/>
                                                                                457R6 Quelque-Part-Jupiter
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>
                                                    <li class="link-telephone">
                                                        <a href="#"><!--telephone-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fas fa-phone"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                (0_O)\0_\0/_0(T_T)
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                                                 
                                                    <li class="link-mail">
                                                        <a href="#"><!--mail-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fas fa-envelope"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                ne-pas-repondre@fake-news.info
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                                                 
                                                </ul>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-12">
                                                <ul class="links-reseaux">
                                                    <li class="link-mail">
                                                        <a href="#"><!--twitter-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fab fa-twitter"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                @Fakenews
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                                                 
                                                    <li class="link-instagram">
                                                        <a href="#"><!--instagram-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fab fa-instagram"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                instagram.com/fake-news
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                                                 
                                                    <li class="link-dribble">
                                                        <a href="#"><!--instagram-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fas fa-basketball-ball"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                dribble.com/fake-news
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                    
                                                    <li class="link-facebook">
                                                        <a href="#"><!--instagram-->
                                                            <div class="row">
                                                                <div class="col-lg-12">
                                                                    <div class="row">
                                                                        <div class="col-lg-2 col-md-2 col-2 col-sm-2">
                                                                            <i class="fab fa-facebook-f"></i>
                                                                        </div>
                                                                        <div class="col-lg-10 col-md-10 col-10 col-sm-10">
                                                                            <span href="#" class="mn-menu-link">
                                                                                facebook.com/fake-news
                                                                            </span>
                                                                        </div>                                                                      
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </li>                                                
                                                </ul>
                                            </div>
                                        </div><!--Fin row Liens -->
                                    </div><!--Fin col liens -->
                                </div><!--Fin row Liens -->
                            </div><!--Fin col general Liens pratiques -->
                        </div><!--Fin row general Liens pratiques -->
                    </div><!-- Fin liens pratique -->
                </div>
                <div class="cont-illegales">
                    <ul>
                        <li>
                            <span>@Fake News. Tous droits réservés.</span>
                        </li>
                        <li>
                            <a href="/php/pages/mts_illegales.php?id=888">
                                Mentions Illégales
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>    

</footer>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
<script type="application/javascript" src="../../js/script.js"></script>
</body>

</html>