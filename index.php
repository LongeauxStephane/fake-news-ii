<?php 
    include 'php/includes/header.php';
    include 'php/includes/functions.php';   
?>

<body>
    


    <!--Titre-->
    <div class="cont-title">
        <h1>Fake News II</h1>
        <span class="ss-titre titre">
            <?= $ss_title_accueil ?>
        </span>
    </div>

    <!--Separation-->
    <div class="separateur sep-hd-main"></div>

    <main class="pg-accueil">

        <h2 class="titre">Les dernières <strong>Fake News !</strong></h2>

        <div class="cont-articles">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <form action="" method="POST" class="form-home-art">
                            <?php articles_list('articles'); ?>
                        </form>                        
                    </div>
                </div>
            </div>         
        </div>
        <div class="cont-btn">
            <a href="php/pages/truc_toc.php" class="btn-art">
                <span>
                    J'en veux encore !
                </span>
            </a>
        </div>

    </main>    
   
</div><!-- Fin constainer -->

<div class="container-fluid"><!-- Emile -->

    <!--separation-->
    <div class="separateur"></div> 

    <div class="cont-emile">
        <span>
            <em>
                "On peut tromper une fois Mille
                personnes,<br/>
                mais on ne peut pas tromper Mille
                fois une <br/> personne." - Émile
            </em>
            
        </span>
    </div>
 
</div>


<?php 
    include 'php/includes/footer.php';
?>


</body>